import React, {useState, useEffect} from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {
    TextField,
    Button,
    List,
    ListItem,
    ListItemText,
    CircularProgress
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    wrapForm: {
        display: "flex",
        justifyContent: "center",
        width: "95%",
        margin: `${theme.spacing(0)} auto`
    },
    wrapText: {
        width: "100%"
    },
    button: {
        //margin: theme.spacing(1),
    },
    root: {
        width: '100%',
        maxWidth: 360,
        justifyContent: "center",
        backgroundColor: theme.palette.background.paper,
    }

}));

//一覧するデータを状態Stateとして設定(内容はWebAPIより取得する)
const initialItems = [];

const Chat = () => {
    const classes = useStyles();
    //チャットの一覧データを保持する状態フック
    //状態フックは状態の一つとなるプロパティ（ここでは初期データ）とその値を変更できる関数のペアを書く。
    const [items, setItems] = useState(initialItems);

    //チャットの投稿フォームに入力されたデータを保持する状態フック
    const [message, setMessage] = useState({
        //jsonデータの要素を記述
        username: '', comment:''
    })

    //投稿主の名前を保持するイベント処理
    const handleInputNameChange = event => {
        //イベントの変更処理
        const value = event.target.value;
        //messageに新しいオブジェクト(名前、メッセの入力内容)を追加
        const newName = {...message};
        //新しいユーザー名の追加をjsonファイルのusernameに書き足す
        newName.username = value;
        //処理内容の変更(追加)をデータベース(jsonデータ)に反映する
        setMessage(newName);
    }
　　// 投稿されたコメントを保持するイベント処理
    const handleInputCommentChange = event => {
        const value = event.target.value;
        const newComment = {...message};
        newComment.comment = value;
        setMessage(newComment);
    }
　　//投稿されたコメントを追加し、一覧に表示するイベント処理
    const handleAddComment = () => {
        updateWebApi({...message})
    }

    //webapiに更新後のデータを
    const updateWebApi = (item) => {
        window.fetch(`http://localhost:8080/posts`, {
            method: 'POST',
            body: JSON.stringify(item),
            headers: {'Content-Type': 'application/json'}
        }).then(res => res.json())
            .then(jsonData => {
                setItems([...items, jsonData]);
                setMessage({
                    username: '', comment: ''
                })
            })
            .catch(error => console.error('データを更新できませんでした :', error));
    };

    useEffect(() => {
        window.fetch('http://localhost:8080/posts?_expand=username')
            .then((data) => data.json())
            .then((json) => {
                setItems([...json]);
            })
            .catch(() => console.error('誰もまだコメントしていません'));
    }, [])


    return (


                <React.Fragment>
                    <form className={classes.wrapForm} noValidate autoComplete="off">
                        <TextField
                            className={classes.wrapText}
                            label="名前を入力"
                            onChange={handleInputNameChange}
                            value={message.username}/>

                        <TextField
                            name="comment"
                            id="standard-text"
                            label="メッセージを入力"
                            className={classes.wrapText}
                            onChange={handleInputCommentChange}
                            value={message.comment}
                        />
                        <Button variant="contained" color="primary" className={classes.button} onClick={handleAddComment}>
                            投稿
                        </Button>
                    </form>
                    <List className={classes.root}>
                        {/*一覧データの配列をmap関数でJSXに変換する*/}
                        {items.map(item =>(
                        <ListItem key={item.id}>
                            <ListItemText primary={item.username} secondary={item.comment}/>
                        </ListItem>
                        ))}
                    </List>
                </React.Fragment>



                    );
                    };
                    export default Chat;