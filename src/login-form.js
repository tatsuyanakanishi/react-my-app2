import React from "react";
import {Button} from "@material-ui/core";

const LoginForm = () => {
    return(
        <Button variant='contained' color='primary'>
            ログイン
        </Button>
    );
};
export default LoginForm;